import requests
from django.core.management.base import BaseCommand

from challenge.models import Currency


class Command(BaseCommand):
    help = 'Fetch the currencies'

    def handle(self, *args, **options):
        # GET https://api.vatcomply.com/currencies HTTP/1.1
        """
        {
          "EUR": {
            "name": "Euro",
            "symbol": "€"
          },
          "USD": {
            "name": "US Dollar",
            "symbol": "$"
          },
          "JPY": {
            "name": "Japanese Yen",
            "symbol": "¥"
          },
          ...
          "BRL": {
            "name": "Brazilian Real",
            "symbol": "R$"
          },
          ...
          "ZAR": {
            "name": "South African Rand",
            "symbol": "ZAR"
          }
        }
        """
        response = requests.get(f'https://api.vatcomply.com/currencies')
        json_data = response.json()
        for abbreviation, value_data in json_data.items():
            name = value_data['name']
            symbol = value_data['symbol']
            obj, created = Currency.objects.get_or_create(
                name=name,
                abbreviation=abbreviation,
                symbol=symbol
            )
