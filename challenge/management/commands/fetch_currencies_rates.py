from datetime import date, timedelta
from decimal import Decimal

import requests
from django.core.management.base import BaseCommand, CommandError

from challenge.models import Currency, CurrencyRate


class Command(BaseCommand):
    help = 'Fetch the currencies rates'

    def handle(self, *args, **options):
        base_currencies = Currency.objects.filter(is_base=True)
        if not base_currencies.exists():
            raise CommandError('Define a Currency base')

        for currency in base_currencies:
            # GET https://api.vatcomply.com/rates?base=USD HTTP/1.1
            # GET https://api.vatcomply.com/rates?date=2000-04-05 HTTP/1.1
            # GET https://api.vatcomply.com/rates?date=1999-01-04&base=USD
            """
            {
                "date": "2000-04-05",
                "base": "EUR",
                "rates": {
                    "EUR": 1,
                    "USD": 0.9673,
                    ...
                    "ZAR": 6.3808
                }
            }
            """
            today = date.today()
            base_date = today + timedelta(-1)
            self.fetch_currency_rates(base_date, currency)

    def fetch_currency_rates(self, base_date, currency):
        if CurrencyRate.objects.filter(currency_base=currency, date=base_date).exists():
            return

        print(f'Fetch... {base_date} {currency}')
        response = requests.get(f'https://api.vatcomply.com/rates?date={base_date}&base={currency.abbreviation}')
        json_data = response.json()
        for abbreviation, rate in json_data['rates'].items():
            currency_to = Currency.objects.get(abbreviation=abbreviation)
            rate = Decimal(rate)
            obj, created = CurrencyRate.objects.get_or_create(
                currency_base=currency,
                currency_to=currency_to,
                date=base_date,
                defaults={
                    'rate': rate,
                }
            )
        self.fetch_currency_rates(base_date + timedelta(-1), currency)

