# -*- coding: utf-8 -*-
from django import forms

from challenge.models import Currency


class ChallengeForm(forms.Form):
    currency = forms.ModelChoiceField(Currency.objects.filter(is_base=True), label='Moeda', required=True)
    date_from = forms.DateField(label='De', widget=forms.DateInput(attrs={'type': 'date'}))
    date_to = forms.DateField(label='Até', widget=forms.DateInput(attrs={'type': 'date'}))

    def clean(self):
        cleaned_data = super(ChallengeForm, self).clean()
        date_from = cleaned_data['date_from']
        date_to = cleaned_data['date_to']
        diff_days = (date_to-date_from).days
        if not (0 <= diff_days < 7):
            self.add_error('date_from', 'O período deve ser de no máximo 5 dias úteis.')
        return cleaned_data

    def process(self):
        results = {}
        cleaned_data = self.cleaned_data
        currency = cleaned_data['currency']
        date_from = cleaned_data['date_from']
        date_to = cleaned_data['date_to']
        currency_rates = currency.rates_base.filter(date__range=[date_from, date_to]).order_by('date')
        for currency_base in Currency.objects.filter(is_base=True).exclude(id=currency.id):
            currency_rates_abb = currency_rates.filter(currency_to=currency_base)
            currency_rates_abb = currency_rates_abb.values_list('rate', flat=True)
            results[currency_base] = list(currency_rates_abb)

        dates = list(currency_rates.values_list('date', flat=True).distinct())
        return dates, results
