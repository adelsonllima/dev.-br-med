from django.contrib import admin

from challenge.models import Currency, CurrencyRate


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'abbreviation', 'symbol', 'is_base')
    ordering = ('id',)


@admin.register(CurrencyRate)
class CurrencyRateAdmin(admin.ModelAdmin):
    list_display = ('currency_base', 'currency_to', 'rate', 'date')
    ordering = ('id',)
