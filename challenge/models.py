from django.db import models


class Currency(models.Model):
    name = models.CharField(verbose_name='Nome', max_length=255)
    abbreviation = models.CharField(verbose_name='Sigla', max_length=5)
    symbol = models.CharField(verbose_name='Símbolo', max_length=5)
    is_base = models.BooleanField(verbose_name='É moeda Base', default=False)

    def __str__(self):
        return f'{self.name} ({self.abbreviation})'


class CurrencyRate(models.Model):
    currency_base = models.ForeignKey(Currency, verbose_name='Moeda Base', on_delete=models.CASCADE, related_name='rates_base')
    currency_to = models.ForeignKey(Currency, verbose_name='Moeda', on_delete=models.CASCADE, related_name='rates_to')
    rate = models.DecimalField(verbose_name='Taxa', max_digits=30, decimal_places=20)
    date = models.DateField(verbose_name='Data')
