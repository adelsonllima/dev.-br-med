from rest_framework import serializers

from challenge.models import Currency, CurrencyRate


class CurrencySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Currency
        fields = '__all__'


class CurrencyRateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CurrencyRate
        fields = '__all__'