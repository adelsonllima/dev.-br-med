import json
from datetime import date, timedelta

from django.shortcuts import render
from rest_framework import viewsets

from challenge.forms import ChallengeForm
from challenge.models import Currency, CurrencyRate
from challenge.serializers import CurrencySerializer, CurrencyRateSerializer


def index(request):
    default_data = {
        'currency': Currency.objects.get(abbreviation='USD').id,
        'date_from': date.today() + timedelta(-7),
        'date_to': date.today() + timedelta(-1),
    }
    form = ChallengeForm(request.POST or default_data)
    if form.is_valid():
        dates, currency_rates = form.process()
        currency = form.cleaned_data['currency']
        date_from = form.cleaned_data['date_from']
        date_to = form.cleaned_data['date_to']
        title = f'Currency Rate {currency} from {date_from} to {date_to}'
        series = []
        for currency_base in Currency.objects.filter(is_base=True).exclude(id=currency.id):
            series.append({
                'name': currency_base.abbreviation,
                'data': [float(rate) for rate in currency_rates[currency_base]],
            })
        date_period = json.dumps([str(date) for date in dates])

    return render(request, 'challenge/index.html', locals())


class CurrencyViewSet(viewsets.ModelViewSet):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer


class CurrencyRateViewSet(viewsets.ModelViewSet):
    queryset = CurrencyRate.objects.all()
    serializer_class = CurrencyRateSerializer
