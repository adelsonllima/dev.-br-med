# DESAFIO DEV. BR MED

## Acesso ao sistema:

1 - Página inicial:
> http://192.241.135.70/

2 - Acesso a parte administrativa:
> Username: admin
> 
> Password: senha
> 
> http://192.241.135.70/admin/

---

## Acesso à API:

> http://192.241.135.70/api/
 
### Documentação:

1 - swagger:
> http://192.241.135.70/swagger/

2 - Redoc:
> http://192.241.135.70/redoc/
